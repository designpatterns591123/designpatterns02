﻿using task5.Interfaces;
using task5.Models;

IHeroBuilder enemyBuilder = new EnemyBuilder();
HeroDirector director = new(enemyBuilder);

director.ConstructVillain("Evil Villain", "Average", "Slim", "Black", "Red", "Dark Robe", ["Magic Staff", "Dildo"]);

Hero villain = enemyBuilder.Build();

Console.WriteLine("Villain:");
villain.DisplayInfo();