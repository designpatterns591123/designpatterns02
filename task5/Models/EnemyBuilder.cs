﻿using System.Collections.Generic;
using task5.Interfaces;

namespace task5.Models
{
	public class EnemyBuilder : IHeroBuilder
	{
		private Hero hero = new();

		public IHeroBuilder SetName(string name)
		{
			hero.Name = name;
			return this;
		}

		public IHeroBuilder SetHeight(string height)
		{
			hero.Height = height;
			return this;
		}

		public IHeroBuilder SetBuild(string build)
		{
			hero.Build = build;
			return this;
		}

		public IHeroBuilder SetHairColor(string hairColor)
		{
			hero.HairColor = hairColor;
			return this;
		}

		public IHeroBuilder SetEyeColor(string eyeColor)
		{
			hero.EyeColor = eyeColor;
			return this;
		}

		public IHeroBuilder SetClothing(string clothing)
		{
			hero.Clothing = clothing;
			return this;
		}

		public IHeroBuilder SetType(string type)
		{
			hero.Type = type;
			return this;
		}

		public IHeroBuilder AddToInventory(string item)
		{
			hero.Inventory ??= [];

			hero.Inventory.Add(item);
			return this;
		}

		public Hero Build()
		{
			return hero;
		}
	}
}
