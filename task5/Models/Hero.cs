﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task5.Models
{
	public class Hero
	{
		public string? Name { get; set; }
		public string? Height { get; set; }
		public string? Build { get; set; }
		public string? HairColor { get; set; }
		public string? EyeColor { get; set; }
		public string? Clothing { get; set; }
		public List<string>? Inventory { get; set; }
		public string? Type { get; internal set; }

		public void DisplayInfo()
		{
			Console.WriteLine($"Name: {Name}");
			Console.WriteLine($"Type: {Type}");
			Console.WriteLine($"Height: {Height}");
			Console.WriteLine($"Build: {Build}");
			Console.WriteLine($"Hair Color: {HairColor}");
			Console.WriteLine($"Eye Color: {EyeColor}");
			Console.WriteLine($"Clothing: {Clothing}");
			Console.WriteLine("Inventory:");
			foreach (var item in Inventory)
			{
				Console.WriteLine($"- {item}");
			}
		}
	}
}
