﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
	public class Authenticator
	{
		private static Authenticator? instance;
		private Authenticator() { }

		public static Authenticator GetInstance()
		{
			instance ??= new Authenticator();
			return instance;
		}

		public static void TestMethod()
		{
			Console.WriteLine("Метод класу Authenticator був викликаний.");
		}
	}
}
