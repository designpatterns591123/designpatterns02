﻿using task3;

Authenticator instance1 = Authenticator.GetInstance();
Authenticator instance2 = Authenticator.GetInstance();

if (instance1 == instance2)
{
	Console.WriteLine("Обидва екземпляри є тим самим об'єктом.");
}
else
{
	Console.WriteLine("Помилка: було створено більше одного екземпляра.");
}

Authenticator.TestMethod();
