﻿using task4.Models;

Virus originalVirus = new Virus(1.5, 1, "COVID-19", "Coronavirus");
originalVirus.Children.Add(new Virus(1.3, 1, "Delta", "Coronavirus"));
originalVirus.Children.Add(new Virus(1.2, 1, "Omicron", "Coronavirus"));

Virus clonedVirus = (Virus)originalVirus.Clone();

clonedVirus.Name = "Cloned COVID-19";
clonedVirus.Children[0].Name = "Cloned Delta";

Console.WriteLine("Original Virus:");
originalVirus.DisplayInfo();
Console.WriteLine("\nCloned Virus:");
clonedVirus.DisplayInfo();
