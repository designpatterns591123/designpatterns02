﻿using task2.Interfaces;
using task2.Models;

IDeviceFactory laptopFactory = new LaptopFactory();
IDevice laptop = laptopFactory.CreateDevice("IProne", "Probook");
laptop.DisplayInfo();

IDeviceFactory smartphoneFactory = new SmartphoneFactory();
IDevice smartphone = smartphoneFactory.CreateDevice("Balaxy", "S22");
smartphone.DisplayInfo();
