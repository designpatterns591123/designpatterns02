﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task1.Interfaces;

namespace task1.Models
{
	public class EducationalSubscription : ISubscription
	{
		public decimal MonthlyFee { get; private set; }
		public int MinimumSubscriptionPeriod { get; private set; }
		private readonly List<string> channels;
		private readonly List<string> features;

		public EducationalSubscription()
		{
			MonthlyFee = 19.99m;
			MinimumSubscriptionPeriod = 6;
			channels = ["Educational Courses", "Tutorials"];
			features = ["Ad-free Experience", "Interactive Quizzes"];
		}

		public List<string> GetChannels()
		{
			return channels;
		}

		public List<string> GetFeatures()
		{
			return features;
		}
		public void DisplayInfo()
		{
			Console.WriteLine("Educational Subscription: Access to educational materials.");
			Console.WriteLine($"Monthly Fee: {MonthlyFee}");
			Console.WriteLine("Available channels:");
			foreach (string channel in channels)
			{
				Console.WriteLine(channel);
			}
			Console.WriteLine("Available features:");
			foreach (string feature in features)
			{
				Console.WriteLine(feature);
			}
			Console.WriteLine("\n\n");
		}
	}
}
