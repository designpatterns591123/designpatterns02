﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1.Interfaces
{
	public interface ISubscription
	{
		decimal MonthlyFee { get; }
		int MinimumSubscriptionPeriod { get; }

		List<string> GetChannels();
		List<string> GetFeatures();

		void DisplayInfo();
	}
}
