﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task1.Interfaces;
using task1.Models;

namespace task1.Utils
{
	public class SubscriptionFactory
	{
		public static ISubscription GetSubscription(string subscriptionType)
		{
			return subscriptionType.ToLower() switch
			{
				"premium" => new PremiumSubscription(),
				"educational" => new EducationalSubscription(),
				"domestic" => new DomesticSubscription(),
				_ => throw new ArgumentException("Unknown subscription type."),
			};
		}
	}

}
